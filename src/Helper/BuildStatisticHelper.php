<?php

namespace Logger\Helper;

/**
 * Помошник для сбора элементов
 */
class BuildStatisticHelper
{
	/**
	 * Сборка представления элемента статистики
	 *
	 * @param string $countryCode
	 * @param int    $count
	 *
	 * @return array
	 */
	public static function buildElement(string $countryCode, int $count): array
	{
		return [$countryCode => $count];
	}
}
