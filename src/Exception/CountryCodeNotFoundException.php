<?php

namespace Logger\Exception;

use Exception;

/**
 * Исключение, код страны не найден, свидетельствует о том, что пользователь присылает корерктный код, но не существующей страны.
 */
class CountryCodeNotFoundException extends Exception
{
	public function __construct()
	{
		parent::__construct(
			'Код страны не найден в списке допустимых',
			ExceptionCode::COUNTRY_CODE_NOT_FOUND_EXCEPTION,
		);
	}
}
