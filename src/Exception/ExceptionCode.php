<?php

namespace Logger\Exception;

/**
 * Класс список сквозных исключений гинерируемых сервисом
 */
class ExceptionCode
{
	/** Код страны, отсутствует в списке известных */
	public const COUNTRY_CODE_NOT_FOUND_EXCEPTION = 1;
}
