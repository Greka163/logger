<?php

namespace Logger\Infrastructure;

use Redis;

/**
 * Оболочка над подключением к Redis
 */
class RedisConnection
{
	private static $instances;

	/** @var Redis */
	private $connection;

	protected function __construct()
	{
	}

	protected function __clone()
	{
	}

	public function __wakeup()
	{
		throw new \Exception("Cannot unserialize a singleton.");
	}

	public static function getInstance(): RedisConnection
	{
		$class = static::class;
		if (!isset(self::$instances[$class])) {
			self::$instances[$class] = new static();
		}

		return self::$instances[$class];
	}

	public function getConnection(): Redis
	{
		if ($this->connection === null) {
			$this->connection = new Redis;
			$this->connection->connect('redis', 6379);
		}
		return $this->connection;
	}
}
