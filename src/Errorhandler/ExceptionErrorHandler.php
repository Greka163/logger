<?php

use Logger\Exception\CountryCodeNotFoundException;

/**
 * Обработчик не отловленных исключений
 * Преобразует исключение в json, выставляет корректный статус ответа.
 *
 * @param Throwable $e
 */
function exceptionHandler(Throwable $e)
{
	header('Content-Type: application/json');
	$message['message'] = $e->getMessage();
	if ($e instanceof CountryCodeNotFoundException) {
		http_response_code(400);
		//TODO лог новых стран уровень warning, переодичность раз в час.
	} else if ($e instanceof InvalidArgumentException) {
		http_response_code(400);
	} else {
		http_response_code(500);
		//TODO лог уровень alert, переодичность раз в в 10 минут.
		$message['message'] = 'Internal server error in the process of repair';
	}

	echo json_encode($message, false);
}

//Подключение  отлова ошибок
set_exception_handler("exceptionHandler");

