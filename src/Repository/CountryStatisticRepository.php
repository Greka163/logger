<?php

namespace Logger\Repository;

use Exception;
use Logger\Helper\CountryHelper;
use Logger\Infrastructure\RedisConnection;

class CountryStatisticRepository
{
	private RedisConnection $connection;
	private const STATISTIC_KEY = 'statistic';

	public function __construct(RedisConnection $connection)
	{
		$this->connection = $connection;
	}

	/**
	 * Сохранение события посещения
	 *
	 * @param string $countryCode
	 *
	 * @throws Exception
	 * @see CountryHelper::getCountryCodes()
	 */
	public function save(string $countryCode): void
	{
		$this->connection->getConnection()->HINCRBY(self::STATISTIC_KEY,$countryCode,1);
	}

	/**
	 * Получение общей статистики
	 */
	public function getStatistics(): array
	{
		return $this->connection->getConnection()->HGETALL(self::STATISTIC_KEY);
	}
}
