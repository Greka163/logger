<?php

namespace Logger\Action;

use Exception;
use InvalidArgumentException;
use Logger\Exception\CountryCodeNotFoundException;
use Logger\Helper\CountryHelper;

/**
 * Action для зодания записи о посещении
 *
 * @link https://petstore.swagger.io/#/pet/addPet
 */
class CreateAction extends AbstractStatisticAction
{
	private const PARAMETER_COUNTRY_CODE = 'c';
	private const PATTERN_COUNTRY_CODE = '^[A-Z]{2}$';
	private const VALIDATE_EXCEPTION_TEXT = 'Parameter code has an invalid value. Permissible value ' .
	self::PATTERN_COUNTRY_CODE;

	/**
	 * @throws Exception
	 */
	public function run(): void
	{
		$code = $this->getCountryCode();
		$this->getRepository()->save($code);
		header_remove();
		http_response_code(201);
	}

	/**
	 * Получаем код страны из запроса
	 *
	 * @throws InvalidArgumentException
	 **/
	private function getCountryCode(): string
	{
		$code = $_GET[self::PARAMETER_COUNTRY_CODE];
		$code = trim($code);
		$code = mb_strtoupper($code);

		if (preg_match('#' . self::PATTERN_COUNTRY_CODE . '#', $code) !== 1) {
			throw new InvalidArgumentException(self::VALIDATE_EXCEPTION_TEXT);
		}

		return $code;
	}
}
