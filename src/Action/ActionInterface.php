<?php

namespace Logger\Action;

/**
 * Интерфейс действий
 */
interface ActionInterface
{
	/**
	 * Запуск действия
	 */
	public function run();
}
