<?php

namespace Logger\Action;

/**
 * Action получения результатов статистики
 *
 * @link https://petstore.swagger.io/#/pet/addPet
 */
class GetAction extends AbstractStatisticAction
{
	public function run(): void
	{
		header_remove();
		header('Content-type:application/json;charset=utf-8');
		echo json_encode($this->getRepository()->getStatistics(), JSON_THROW_ON_ERROR);
	}
}
