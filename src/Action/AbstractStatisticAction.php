<?php

namespace Logger\Action;

use Logger\Repository\CountryStatisticRepository;

abstract class AbstractStatisticAction implements ActionInterface
{
	private CountryStatisticRepository $repository;

	public function __construct(CountryStatisticRepository $repository)
	{
		$this->repository = $repository;
	}

	public function getRepository(): CountryStatisticRepository
	{
		return $this->repository;
	}
}
