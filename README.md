Отдельное спасибо владельцу этого [репозитория](https://github.com/albertcolom/docker-lemp/tree/master), помог сократить время на разворачивание Redis

Docker LEMP + Redis
===========================
### Docker multicontainer: Nginx, php7-fpm, MySQL, Redis

### Requirements
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### Installation
## git clone 

```
mkdir logger;
git clone https://gitlab.com/Greka163/logger.git logger
```

Start docker compose
```sh
$ docker-compose up -d
```
## composer
```
docker exec -i -t logger_php_1 /bin/bash 
# в контейнере запустить
./composer.phar install 
```
Web Server
- [http://localhost:8080](http://localhost:8080)

Stop containers docker compose
```sh
$ docker-compose stop
```

Remove containers docker compose
```sh
$ docker-compose rm -f
```

### Обновление статистики

## Комментарии к задаче

[Заметки и комментарии](https://docs.google.com/document/d/1kv0rRNNCSSX_PVNbS1h5GPsvjU_CTWqkM-5HPB5OmW4) 

Метод: POST
```sh
curl --location --request POST 'http://localhost:8080?c=ax'
```
где `?c=ax` – 2х буквенный, регистронезависимый код страны (ru, us, it...) соотвествующий регулярному выражению `^[A-Z]{2}$`
Возможные коды ответов:

201 Created

400 Bad Request
```
{
    "message": "Parameter code has an invalid value. Permissible value ^[A-Z]{2}$"
}
```

500 Internal server error
```
{
    "message": "Internal server error in the process of repair"
}
```
### Получение результатов статистики 
Метод: GET 
```
curl --location --request GET 'http://localhost:8080'
```
Возможные коды ответов:

200 OK
```
{
    "AP": "8",
    "AS": "3",
    "AX": "17"
}
```

500 Internal server error
```
{
    "message": "Internal server error in the process of repair"
}
```

### Структура проекта 
```
- app - входная точка, перечисление доступных action, autoload, подключение отлова ошибок.
- src - код приложения 
-- Action - доступные действия
-- Errorhandler - логика обработки ошибок
-- Exception - исключения генерируемые приложением
-- Helper - классы помошники для обработки данных
-- Infrastructure - инфраструктурные компоненты (BD,Logger и т.п.)
-- Repository - классы для общения с БД
```
