<?php

use Logger\Action\ActionInterface;
use Logger\Action\CreateAction;
use Logger\Action\GetAction;
use Logger\Infrastructure\RedisConnection;
use Logger\Repository\CountryStatisticRepository;

// Название сервиса использется для идентификации сервиса.
define('SERVICE_NAME', 'log_aggregator');
// auto load
require_once '../vendor/autoload.php';
// error handler
require_once '../src/Errorhandler/ExceptionErrorHandler.php';

$methodName = $_SERVER['REQUEST_METHOD'];
switch ($_SERVER['REQUEST_METHOD']) {
	case 'POST':
		$action = new CreateAction(new CountryStatisticRepository(RedisConnection::getInstance()));
		break;
	case 'GET':
		$action = new GetAction(new CountryStatisticRepository(RedisConnection::getInstance()));
		break;
	default:
		throw new LogicException($methodName . ' не допустим в сервисе' . SERVICE_NAME);
}

if ($action instanceof ActionInterface) {
	$action->run();
}
